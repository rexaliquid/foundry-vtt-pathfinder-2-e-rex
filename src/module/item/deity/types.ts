import enJSON from "../../../../static/lang/en.json";

type DeityDomain = keyof typeof enJSON["PF2E"]["Item"]["Deity"]["Domain"];

export { DeityDomain };
